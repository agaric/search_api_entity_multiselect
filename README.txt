Creates multiselect form field for entities following the pattern used by search_api for taxonomy vocabularies. 

Depends on https://git.drupalcode.org/project/drupal/-/merge_requests/4053.diff
