<?php

namespace Drupal\search_api_entity_multiselect\Plugin\views\filter;

use Drupal\views\Plugin\views\filter\EntityReference;
use Drupal\views\Plugin\views\filter\ManyToOne;
use Drupal\views\ViewExecutable;
use Drupal\views\Plugin\views\display\DisplayPluginBase;

/**
 * Defines a filter for filtering on taxonomy term references.
 *
 * Note: The plugin annotation below is not misspelled. Due to dependency
 * problems, the plugin is not defined here but in
 * search_api_views_plugins_filter_alter().
 *
 * @ingroup views_filter_handlers
 *
 * ViewsFilter("search_api_term")
 *
 * @see search_api_views_plugins_filter_alter()
 */
class SearchApiEntity extends EntityReference {

  use \Drupal\search_api\Plugin\views\filter\SearchApiFilterTrait;

    /**
   * {@inheritdoc}
   */
  public function init(ViewExecutable $view, DisplayPluginBase $display, array &$options = NULL): void {
    
    if (empty($this->definition['entity_type'])) {
      $this->definition['entity_type'] = 'node';
      $this->definition['field_name'] = 'field_organization';
      $this->options['handler'] = 'views';
      $this->options['sub_handler'] = 'views';
    }

    parent::init($view, $display, $options);
  }


    /**
   * {@inheritdoc}
   */
  protected function defineOptions(): array {
    $options = ManytoOne::defineOptions();

    $options['sub_handler_settings'] = ['default' => []];
    $options['widget'] = ['default' => static::WIDGET_AUTOCOMPLETE];

    return $options;
  }

}
